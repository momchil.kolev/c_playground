#include <stdio.h>
#include <string.h>

/*
 * & - returns the address of a value
 * * - returns the value of an address
 *    
 * When used during declaration
 * * - create a pointer
 * & - create an alias (available in C++, but not in C)
*/

typedef struct _word
{
    char *value;
    struct _word *next;
} WORD;

WORD create_linked_list(char *words);

int main(void)
{

    // char words[] = "Words and things and stuff .";
    // Get a string of characters up to 256 bytes
    char words[256];
    printf("Enter a space-separated list of words: ");
    scanf("%[^\n]s", words);
    // words[strlen(words)] = '\0';

    WORD linked_list = create_linked_list(words);

    // Printf the list
}

// Function to create a linked list
WORD create_linked_list(char *words)
{
    WORD next_word;
    next_word.value = strtok_r(words, " ", &words);
    if (strlen(words) > 0)
    {
        // next_word.next = &create_linked_list(words);
        WORD next = create_linked_list(words);
        next_word.next = &next;
    }
    return next_word;
}